# Peer to Peer Network

A peer to peer network is a network where the client acts as a server. There is no centralized server in a peer to peer to network.

![alt text](nodep2p2.png)

To run the app on a browser I've built it using Node Js.The following npm packages will help in achieving the p2p architecture:

* [SingnalHub](https://www.npmjs.com/package/signalhub) - to tell other peers (clients) to connect with our peer.
* [webRTC-swarm](https://github.com/mafintosh/webrtc-swarm) - SignalHub sends the message to another peer to connect and **webRTC-swarm** makes the connection.
* [simple-peer](https://www.npmjs.com/package/simple-peer) - makes the browser a peer node: provides video, voice, and data channels.

## Project Requirements
For development, you need Node.js and a node package manage - Yarn or npm, installed on your environment.

### Node
- #### Installation on Windows
    Download node installer from the official [Node.js website](https://nodejs.org/en/download/), then run it.

    Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu
    You can install nodejs and npm easily with apt install, just run the following commands.

    ```
    $ sudo apt install nodejs
    $ sudo apt install npm
    ```

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following commands and receive an output with the respective versions

```
$ node --version
v8.11.3
```

```
$ npm --version
6.1.0
```

If you need to update `npm`
```
$ npm install npm -g
```

## Install Project on local 
```
$ git clone https://gitlab.com/arnoldnderitu1/p2p-nodejs-video-chat.git
$ cd p2p-nodejs-video-chat
$ npm install
```

### Run signal hub
```
$ npm run signalhub
```

### Run project on browser
```
$ npm run start
```